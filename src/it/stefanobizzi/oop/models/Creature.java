package it.stefanobizzi.oop.models;

public class Creature {
    private final String name;
    private int health;

    public Creature(final String name, final int initialHealth) {
        this.name = name;
        this.health = initialHealth;
        System.out.println("Creature " + name + " created.");
    }

    public void hit(final Creature target, final int hit) {
        System.out.println(this.getName() + " attacks " + target.getName() + " for " + hit);
        target.react(this, hit);
    }

    protected void react(final Creature attacker, final int hit) {
        System.out.println(attacker.getName() + " attacked " + this.getName() + " for " + hit);
        health -= hit;
        System.out.println("Remaining health " + health);
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    protected void setHealth(int health) {
        this.health = health;
    }

}













