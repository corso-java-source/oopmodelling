package it.stefanobizzi.oop.models;

import it.stefanobizzi.oop.models.itembase.Drink;
import it.stefanobizzi.oop.models.itembase.Food;

public class Main {
    public static void main(String[] args) {
        Monster bear = new Monster("Bear", 150);
        Player player = new Player("Player 1", 100, "Google");
        bear.hit(player, 40);
        player.hit(bear, 30);
        player.consume(new Food("bread"));
        player.consume(new Drink("beer"));
//        System.out.println(player instanceof Creature);
//        System.out.println(player instanceof Monster);
//        Creature playerTwo = new Player("Player 2", 100, "test");
//        System.out.println(playerTwo instanceof Player);
//        System.out.println(playerTwo instanceof Monster);
    }
}
