package it.stefanobizzi.oop.models;

public class Monster extends Creature {
    public Monster(final String name, final int initialHealth) {
        super(name, initialHealth);
        System.out.println("Monster created");
    }

    @Override
    public void hit(Creature target, int hit) {
        System.out.println(this.getName() + " roars!!!");
        super.hit(target, hit);
    }
}
