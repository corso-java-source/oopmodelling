package it.stefanobizzi.oop.models;

import it.stefanobizzi.oop.models.itembase.Drink;
import it.stefanobizzi.oop.models.itembase.Food;

public class Player extends Creature {
    private String userAccount;

    public Player(final String name, final int initialHealth, final String userAccount) {
        super(name, initialHealth);
        this.userAccount = userAccount;
        System.out.println("Player created");
    }

    public void consume(Food food) {
        System.out.println(this.getName() + " eats " + food.getFoodType());
    }

    public void consume(Drink drink) {
        System.out.println(this.getName() + " drinks " + drink.getDrinkType());
    }

    @Override
    public void hit(Creature target, int hit) {
        System.out.println(this.getName() + " swings his sword");
        super.hit(target, hit);
        System.out.println(this.getName() + " gets back to guard");
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }
}
