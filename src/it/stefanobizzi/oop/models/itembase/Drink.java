package it.stefanobizzi.oop.models.itembase;

public class Drink {
    private String drinkType;

    public Drink(final String drinkType) {
        this.drinkType = drinkType;
    }

    public String getDrinkType() {
        return drinkType;
    }
}
