package it.stefanobizzi.oop.models.itembase;

public class Food {
    private String foodType;

    public Food(final String foodType) {
        this.foodType = foodType;
    }

    public String getFoodType() {
        return foodType;
    }
}
